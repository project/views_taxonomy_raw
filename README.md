Views Raw Taxonomy Fields (D7)
==============================
This module adds a new "Taxonomy term: Term description (Raw)" field to Views in
Drupal 7, so that the raw, unfiltered value of a taxonomy term can be exported
or rendered in a View.
